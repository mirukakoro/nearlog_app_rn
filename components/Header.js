import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import styles from './Styles';

const Header = props => {
  return (
    <View style={styles.container}>
      <Text style={(styles.text, styles.big)}>{props.title}</Text>
    </View>
  );
};

export default Header;
