import React from 'react';
import {View, Text, Switch, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const Header = props => {
  return (
    <View style={(styles.container, {backgroundColor: 'darkblue', height: 65})}>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          padding: 10,
        }}>
        <Text style={{color: 'white', fontSize: 30}}>{props.title}</Text>
      </View>
    </View>
  );
};

const SwitchRow = props => {
  return (
    <View style={(styles.container, styles.whiteBack)}>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          paddingTop: 20,
        }}>
        <Switch />
        <Icon
          name={(styles.whiteBack, props.icon)}
          size={20}
          color="darkblue"
        />
        <Text style={{color: 'darkblue', fontSize: 20}}>{" "}{props.title}</Text>
      </View>
    </View>
  );
};

const SectionHeader = props => {
  return (
    <View style={(styles.container, styles.whiteBack)}>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          paddingTop: 20,
        }}>
        <Icon
          name={(styles.whiteBack, props.icon)}
          size={40}
          color="darkblue"
        />
        <Text style={{color: 'darkblue', fontSize: 40}}> {props.title}</Text>
      </View>
      <Text>
        {'\n'}
        {'\n'}
      </Text>
    </View>
  );
};

Header.defaultProps = {
  title: 'Title',
};

SectionHeader.defaultProps = {
  title: 'Title',
  icon: 'checkbox-blank-circle-fix-outline',
};

SectionHeader.defaultProps = {
  title: 'Title',
  icon: 'checkbox-blank-circle-fix-outline',
};

const styles = StyleSheet.create({
  container: {},
  whiteBack: {
    color: 'darkblue',
  },
  darkblueBack: {
    color: 'white',
    backgroundColor: 'darkblue',
  },
  center: {
    textAlign: 'center',
  },
});

export {SectionHeader, SwitchRow, Header, styles};
