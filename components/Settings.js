import React from 'react';
import {View, Text} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './Styles';

// const [isEnabled, setIsEnabled] = useState(false);
// const toggleSwitch = () => setIsEnabled(previousState => !previousState);

const Settings = props => {
  return (
    <View style={styles.container}>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          paddingTop: 20,
        }}>
        <Icon name="settings-outline" size={40} color="darkblue" />
        <Text style={styles.text}> Settings</Text>
      </View>
      <Text>
        {'\n'}
        {'\n'}
      </Text>
      <Text>
        UNKOLorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a
        sagittis tellus, ac feugiat risus. Integer at ante in tortor dignissim
        efficitur. Suspendisse pretium bibendum metus, at volutpat lacus viverra
        in. Nam euismod lorem sit amet tortor laoreet, et molestie tellus
        faucibus. Duis vel sem nunc. Curabitur enim mauris, tempus vel viverra
        eget, scelerisque nec eros. Cras elementum auctor leo eget sodales. Cras
        sollicitudin, odio sed varius placerat, eros ex efficitur nisl, lobortis
        ullamcorper orci dui sed massa. Cras pretium erat id felis consequat, a
        blandit dolor venenatis. Mauris sit amet velit dictum, interdum eros id,
        facilisis nisi. Phasellus euismod consequat lorem non efficitur. Quisque
        non egestas nulla. Maecenas placerat lectus at libero iaculis, in
        maximus est ornare. Sed faucibus, nunc consectetur gravida placerat,
        lectus enim interdum lectus, nec bibendum ex augue sed diam.
      </Text>
    </View>
  );
};

export default Settings;
